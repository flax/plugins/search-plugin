module.exports = (eleventyConfig, options = {}) => {
  const markdown = require("markdown-it")({
    html: true,
    breaks: true,
    linkify: true,
  });

  const slugify = require('slugify')
  eleventyConfig.addShortcode(
    'flaxSearch', searchFn  
  )

  function searchFn (collectionArr) {

    let inputStr = ` 
    <aside class="inputblock">
    <input id="searchInput" type="text"/>
    <button id="searchIcon">
      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                                                                                         stroke="currentColor" stroke-width="2"  
                                                                                            stroke-linecap="round" stroke-linejoin="round">
        <circle cx="11" cy="11" r="8"></circle>
        <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
      </svg>
    </button>
    <div class="andor">
      <input type="radio" id="and" name="andor" value="and" checked>
      <label for="and">and</label>
      <input type="radio" id="or" name="andor" value="or">
      <label for="or">or</label>
    </div>
    <div class="previousSearch">
      <button class="clearPrevious">clear previous queries</button>
    </div>
    </aside>
    <ul id="results"></ul>
    <template class="searchspace">
    <main class="content">`

    collectionArr.forEach(thisCollection => {
      thisCollection.forEach(item => {
        const title = item.data.title;
        const content = markdown.render(item.templateContent);
        inputStr = ` ${inputStr}
          <section class="data" id=${slugify(title)} >
            <h1>${title}</h1>
            ${content}
          </section>
        `;
      });
    });

    inputStr = ` ${inputStr}</main>
  </template>
  <section class="results">
    <p>Please fill the input to search</p>
  </section>
    `

  return inputStr;
  }
}

