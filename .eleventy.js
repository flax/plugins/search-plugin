const searchPlugin = require('./plugins/.eleventy.js')
module.exports = function (eleventyConfig) {
  eleventyConfig.addPassthroughCopy({ "static/css": "/css" });
  eleventyConfig.addPassthroughCopy({ "static/fonts": "/fonts" });
  eleventyConfig.addPassthroughCopy({ "static/js": "/js" });
  eleventyConfig.addPassthroughCopy({ "static/images": "/images" });
  eleventyConfig.addPassthroughCopy({ "static/outputs": "/outputs" });

  eleventyConfig.addCollection("sortedByOrder", function (collectionApi) {
    return collectionApi.getAll().sort((a, b) => {
      if (a.data.order > b.data.order) return 1;
      else if (a.data.order < b.data.order) return -1;
      else return 0;
    });
  });
  eleventyConfig.addCollection("content1", collectionApi => {
    return collectionApi.getFilteredByGlob("src/content/demosContent1/**/*.md").sort((a, b) => a.data.part - b.data.part);
  });
  eleventyConfig.addCollection("content2", collectionApi => {
    return collectionApi.getFilteredByGlob("src/content/demosContent2/**/*.md").sort((a, b) => a.data.part - b.data.part);
  });


  const markdown = require("markdown-it")({
    html: true,
    breaks: true,
    linkify: true,
  });

  eleventyConfig.addFilter("markdownify", function (rawString) {
    return markdown.render(rawString);
  });

  //import search plugins
  eleventyConfig.addPlugin(searchPlugin);



  // folder structures
  // -----------------------------------------------------------------------------
  // content, data and layouts comes from the src folders
  // output goes to public (for gitlab ci/cd)
  // -----------------------------------------------------------------------------
  return {
    dir: {
      input: "src",
      output: "public",
      includes: "layouts",
      data: "data",
    },
  };
};

